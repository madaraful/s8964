#!/usr/bin/env python3

from s8964 import s8964
import http.server
import urllib.parse

def server(port):
    assert type(port) is int
    assert port >= 0 and port <= 65535

    class handler(http.server.BaseHTTPRequestHandler):
        def do_GET(self):
            if self.path == '/':
                self.send_response(200)

                self.send_header('content-type', 'text/html')
                self.end_headers()

                self.wfile.write(b'''<!DOCTYPE HTML>
                <html>
                    <head>
                        <title>Shortener 8964</title>
                        <meta charset="utf-8"/>
                    </head>
                    <body><center>
                        <h1>Shorter 8964 - A URL Shortener.</h1>
                        <h3>License: GNU GPLv3 or later</h3>
                        <h3><a href="https://gitlab.com/madaraful/s8964">https://gitlab.com/madaraful/s8964</a></h3>
                        <br/><br/>
                        <big><big><form action="/.formapi" method="POST">
                            Long URL:<input name="url" type="text"/> <input type="submit" value="Shorten!"/>
                        </form></big></big>
                    </center></body>
                </html>''')
                return
            else:
                key = self.path[1:]

                s8964a = s8964()
                url = s8964a.get(key)
                if url is None:
                    self.send_response(404)
                    self.send_header('content-type', 'text/html')
                    self.end_headers()
                    self.wfile.write(b'404 content not found')
                    return
                
                self.send_response(301)
                self.send_header('location', url)
                self.end_headers()
                return
        
        def do_POST(self):
            def getdata():
                nonlocal self

                datalen = int(self.headers['content-length'])
                if datalen > 65500:
                    self.send_response(400)

                    self.send_header('content-type', 'text/html')
                    self.end_headers()

                    self.wfile.write(b'url too long')
                    return
                
                data = self.rfile.read(datalen)

                try:
                    data = data.decode('utf-8')
                except BaseException as err:
                    self.send_response(400)

                    self.send_header('content-type', 'text/html')
                    self.end_headers()

                    self.wfile.write(b"post data's encoding is not corrent, it must be UTF-8")
                    raise err
                
                return data
            
            if self.path == '/':
                data = getdata()
                #print(repr(data))

                s8964a = s8964()
                try:
                    key = s8964a.new(data)
                except BaseException as err:
                    self.send_response(400)
                    self.send_header('content-type', 'text/plain')
                    self.end_headers()
                    self.wfile.write(repr(err).encode('utf-8'))
                    raise err

                self.send_response(200)
                self.send_header('content-type', 'text/plain')
                self.end_headers()
                self.wfile.write(b'/' + key.encode('utf-8'))
                return
            elif self.path == '/.formapi':
                data = getdata()
                data = urllib.parse.parse_qs(data)

                url = data['url']
                if len(url) != 1:
                    self.send_response(400)
                    self.send_header('content-type', 'text/html')
                    self.wfile.write(b'wrong number of parameters, or no param')
                    return

                url = url[0]
                s8964a = s8964()
                try:
                    key = s8964a.new(url)
                except BaseException as err:
                    self.send_response(400)
                    self.send_header('content-type', 'text/plain')
                    self.end_headers()
                    self.wfile.write(repr(err).encode('utf-8'))
                    raise err

                self.send_response(200)
                self.send_header('content-type', 'text/html')
                self.end_headers()

                host = self.headers['host']
                self.wfile.write(f'<h3>Your short URL is <a href="/{key}">{host}/{key}</a></h3>'.encode('utf-8'))
                return
            else:
                self.send_response(404)
                self.send_header('content-type', 'text/html')
                self.end_headers()
                self.wfile.write(b'404 action not found')
                return
                

    s = http.server.ThreadingHTTPServer(('127.0.0.1', port), handler)
    return s

def main():
    s = server(8964)
    s.serve_forever()

if __name__ == '__main__': main()
