#!/usr/bin/env python3

import sqlite3
import urllib3
import socket

def url_checker(url):
    return

    url = urllib3.util.parse_url(url)
    print(repr(url))

    if url.scheme != 'http' and url.scheme != 'https' and url.scheme != 'ftp':
        raise Exception('invalid url protocol!')

    if url.host is None:
        raise Exception('invalid url: no hostname!')

KEY_LETTERS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ' # base62 = 10 numbers + 26 lower letters + 26 upper letters
def int2key(n, letters=KEY_LETTERS):
    assert type(n) is int and type(letters) is str

    if n == 0:
        return letters[0]
    if n < 0:
        raise \
        Exception('negative number is not allowed.')

    arr = []
    base = len(letters)
    while n:
        n, rem = divmod(n, base)
        arr.append(letters[rem])
    arr.reverse()

    key = ''.join(arr)
    return key
def key2int(key, letters=KEY_LETTERS):
    assert type(key) is str and type(letters) is str
    base = len(letters)
    keylen = len(key)

    assert keylen > 0

    n = 0

    idx = 0
    for it in key:
        power = (keylen - (idx + 1))
        n += (letters.index(it)) * (base ** power)
        idx += 1

    return n

class s8964():
    # all exist short url mapping is cached
    cache = {}

    def __init__(self):
        # connect to database
        self.db = sqlite3.connect('./s8964_database.sqlite3')

        self.db.execute('CREATE TABLE IF NOT EXISTS map (key TEXT NOT NULL UNIQUE, url TEXT NOT NULL UNIQUE);')
        self.db.commit()

    def _gen(self):
        '''
        generate a new short url's key, collected but now created now.
        '''

        n = self.db.execute('SELECT COUNT(*) FROM map;').fetchone()[0]
        return int2key(n)
    def gen(self):
        '''
        safe version of self._gen
        '''

        left = 10
        while True:
            k = self._gen()

            if len(self.db.execute('SELECT * FROM map WHERE key = ?', (k,)).fetchall()) == 0:
                return k
            else:
                left -= 1

                if left <= 0:
                    raise \
                    BlockingIOError('please retry at later')

            

    def new(self, url):
        '''
        Create a new shorted url.
        '''

        url_checker(url)

        d = self.db.execute('SELECT * FROM map WHERE url = ?;', (url,)).fetchall()
        if len(d) > 0:
            return d[0][0]

        key = self.gen()

        self.db.execute('INSERT INTO map VALUES (?, ?);', (key, url))
        self.db.commit()

        return key

    def get(self, key):
        '''
        to get a exists short url.
        '''

        assert type(key) is str and len(key) > 0

        if key in self.cache.keys():
            return self.cache[key]

        url = self.db.execute('SELECT * FROM map WHERE key = ?;', (key,)).fetchone()
        if url is not None:
            url = url[1]
            self.cache[key] = url
            return url

def test_speed():
    s = s8964()

    import timeit

    def to(k):
        nonlocal s

        print('key:', k)
        for _ in range(2):
            print(timeit.timeit(lambda: s.get(k), number=1))

    to('1')
    print('='*20)
    to('1010222929')

